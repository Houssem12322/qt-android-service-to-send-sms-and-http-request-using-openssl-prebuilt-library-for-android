#ifndef TOOLS_H
#define TOOLS_H

#include <QObject>
#include <QAndroidJniObject>
#include <QAndroidJniEnvironment>
#include <QtAndroid>
#include "QJsonObject"
#include "QJsonDocument"
#include "QFile"
#include "QStandardPaths"
#include "QDebug"
#include "QtNetwork"

class tools : public QObject
{
    Q_OBJECT
public:
    explicit tools(QObject *parent = nullptr);
    static bool internetConection();

signals:
    void configurationState(const QString &msg);

public slots:
    void testService(const QString &getUrl, const QString &postUrl, const QString &day, const QString &passWord, const QString &userPhone, const QString &techPhone);
    void startService(const QString &getUrl, const QString &postUrl, const QString &day, const QString &passWord, const QString &userPhone, const QString &techPhone);
    void startServiceFromOld();
};

#endif // TOOLS_H
