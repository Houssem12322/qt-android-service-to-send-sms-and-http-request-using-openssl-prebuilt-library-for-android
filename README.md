# Qt android service to send sms and http request using openSSL prebuilt library for android

Hybrid android application developed with Qt framework using QAndroidJniObject 'http://doc.qt.io/qt-5/qandroidjniobject.html' to mix android (java) and Qt (C/C++) code.
The aim of this application is to broadcast sms to a list of phone numbers each month. The list is obtained from an API as a Json file using httpRequest. In this project a prebuilt OpenSSL 1.0.0 library is used to establish secure connection to the API (HTTPS).
The code of the API can be found in 'https://gitlab.com/saphidev/tschool.git'

Note that this project is not for direct uses. It need modifications to use it.

For more information feel free to contact me on my email adress 'houssem.merdaci@gmail.com'