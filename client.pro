QT += qml quick
QT += androidextras
QT += quickcontrols2
CONFIG += c++11

SOURCES += client.cpp \
    tools.cpp \

RESOURCES +=\
    main.qml\
    ServiceConfiguration.qml\
    qtquickcontrols2.conf \
    icons/gallery/index.theme\
    icons/gallery/20x20/back.png \
    icons/gallery/20x20/drawer.png \
    icons/gallery/20x20/menu.png \
    icons/gallery/20x20@2/back.png \
    icons/gallery/20x20@2/drawer.png \
    icons/gallery/20x20@2/menu.png \
    icons/gallery/20x20@3/back.png \
    icons/gallery/20x20@3/drawer.png \
    icons/gallery/20x20@3/menu.png \
    icons/gallery/20x20@4/back.png \
    icons/gallery/20x20@4/drawer.png \
    icons/gallery/20x20@4/menu.png

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)
DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    android/src/com/school/ajaxService/MyService.java \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    android/src/com/school/service/MyService.java \
    android/src/com/school/service/MyBroadcastReceiver.java \
    android/src/com/school/service/NotificationReceiver.java \
    android/src/com/school/service/SmsSentReceiver.java \
    android/src/com/school/service/PostRequestThread.java




ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
contains(ANDROID_TARGET_ARCH,armeabi-v7a) {
    ANDROID_EXTRA_LIBS = \
        $$PWD/android/libs/libcrypto.so \
        $$PWD/android/libs/libssl.so
}

HEADERS += \
    tools.h \

