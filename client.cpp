#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QAndroidJniObject>
#include <QAndroidJniEnvironment>
#include <QtAndroid>
#include <QThread>
#include "QJsonObject"
#include "tools.h"
#include "QIcon"
#include "QSettings"
#include <QQuickStyle>



int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    QIcon::setThemeName("gallery");

    QSettings settings;
    QString style = QQuickStyle::name();
    if (!style.isEmpty())
        settings.setValue("style", style);
    else
        QQuickStyle::setStyle(settings.value("style").toString());

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("availableStyles", QQuickStyle::availableStyles());


    tools tool;
    QAndroidJniObject::callStaticMethod<void>("com/kdab/training/MyService",
                                              "requestePermission",
                                              "(Landroid/app/Activity;Landroid/content/Context;)V",
                                              QtAndroid::androidActivity().object(),
                                              QtAndroid::androidContext().object());
    QThread::sleep(4);


    engine.rootContext()->setContextProperty("tools", &tool);

    engine.load(QUrl(QLatin1String("qrc:/main.qml")));



    return app.exec();
}
