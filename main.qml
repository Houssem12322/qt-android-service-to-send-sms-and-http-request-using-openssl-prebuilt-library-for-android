import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.11
import QtQuick.Controls.Material 2.4
import QtQuick.Controls.Universal 2.4
import Qt.labs.settings 1.0

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")


    Shortcut {
        id: escBack
        sequences: ["Esc", "Back"]
        enabled: stackView.depth > 1
        onActivated: {

            stackView.pop()
            titleLabel.text = "Utilisateur"

        }
    }
    Shortcut {
        sequence: "Menu"
        onActivated: optionsMenu.open()
    }
    Settings {
        id: settings
        property string style: "Default"
    }


    header: ToolBar {
        id:toolBar
        Material.foreground: "#757575"
        Material.background: "#ecffffff"
        width: parent.width

        RowLayout {
            spacing: 20
            anchors.fill: parent

            ToolButton {
                id: backPage
                icon.name: "back"
                onClicked: {
                    if (stackView.depth > 1) {
                        stackView.pop()
                        titleLabel.text = "Utilisateur"
                    }
                }
            }


            Label {
                id: titleLabel
                text: "Utilisateur"
                font.pixelSize: 20
                elide: Label.ElideRight
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
            }




            ToolButton {
                icon.name: "menu"
                onClicked: optionsMenu.open()

                Menu {
                    id: optionsMenu
                    x: parent.width - width
                    transformOrigin: Menu.TopRight

                    MenuItem {
                        text: "Utilisateur"
                        onTriggered:{
                                if(titleLabel.text !== "Utilisateur")
                                {
                                    stackView.pop()
                                    titleLabel.text = "Utilisateur"
                                }
                        }
                    }
                    MenuItem {
                        text: "Technicien"
                        onTriggered:{
                            if(titleLabel.text === "Utilisateur")
                            {
                                backPage.visible = true
                                titleLabel.text = "Technicien"
                                stackView.push("qrc:/ServiceConfiguration.qml")
                            }
                        }
                    }
                }
            }
        }
    }


    StackView {
        id: stackView
        focus: true
        anchors.fill: parent


        initialItem:Flickable {
            id: pane



            boundsMovement: Flickable.StopAtBounds
            boundsBehavior: Flickable.DragOverBounds
            opacity: Math.max(0.5, 1.0 - Math.abs(verticalOvershoot) / height)

        }


    }




}
