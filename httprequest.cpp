#include "httprequest.h"


httpRequest::httpRequest(QObject *parent) : QObject(parent)
{
    readConfiguration();
    m_networkRequest.setUrl(QUrl(m_getApi));
    m_networkRequest.setRawHeader(QByteArray("Authorization"), m_passWord.toUtf8());

    m_timer = new QTimer(this);
    m_connectionErrorTimer = new QTimer(this);

    connect(m_timer, &QTimer::timeout, this, &httpRequest::shouldRequest);
    connect(m_connectionErrorTimer, &QTimer::timeout, this, [&](){m_networkManager.get(m_networkRequest);});
    connect(&m_networkManager, &QNetworkAccessManager::finished, this, &httpRequest::treatRequestResult);

    m_connectionAttempts = 0;
    m_timer->start(3600000);
    //m_networkManager.get(m_networkRequest);

}


bool httpRequest::internetConection(){
    QNetworkAccessManager nam;
    QNetworkRequest req(QUrl("http://www.google.com"));
    QNetworkReply *reply = nam.get(req);
    QEventLoop loop;
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();
    if(reply->bytesAvailable())
    {
        return true;
    }
    else
    {
        return false;
    }
}

void httpRequest::shouldRequest()
{
    m_currentDate = QDate::currentDate();
    m_currentTime = QTime::currentTime();
    if(m_currentDate.day() == m_day)
    {
        if(m_currentTime.hour()> 9 && m_currentTime.hour()< 18  )
        {
            m_networkManager.get(m_networkRequest);
            m_timer->stop();
            QTimer::singleShot(86400000, m_timer, SLOT(start()));
        }
    }

}

void httpRequest::treatRequestResult(QNetworkReply* reply)
{
    if(reply->error() != QNetworkReply::NoError){

        if(m_connectionAttempts == 0)
        {
            m_connectionErrorTimer->start(3600000);
        }
        m_connectionAttempts++;
        if(m_connectionAttempts >= 3)
        {
            m_connectionAttempts = 0;
            if(internetConection())
            {
                QAndroidJniObject javaPostApi = QAndroidJniObject::fromString(m_postApi);
                QAndroidJniObject javaPassWord = QAndroidJniObject::fromString(m_passWord);
                QAndroidJniObject javaPhone = QAndroidJniObject::fromString(m_techPhone);
                QAndroidJniObject javaMessage = QAndroidJniObject::fromString("Can't establish a connection to the API");
                QAndroidJniObject::callStaticMethod<void>("com/school/srvice/MyService",
                                                   "sendSms",
                                                   "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V",
                                                   javaPostApi.object<jstring>(),
                                                   javaPassWord.object<jstring>(),
                                                   javaPhone.object<jstring>(),
                                                   javaMessage.object<jstring>());
            }else
            {
                QAndroidJniObject javaPostApi = QAndroidJniObject::fromString(m_postApi);
                QAndroidJniObject javaPassWord = QAndroidJniObject::fromString(m_passWord);
                QAndroidJniObject javaPhone = QAndroidJniObject::fromString(m_userPhone);
                QAndroidJniObject javaMessage = QAndroidJniObject::fromString("Veuillez bien vérifier le mobile de diffusion de SMS");
                QAndroidJniObject::callStaticMethod<void>("com/school/srvice/MyService",
                                                   "sendSms",
                                                   "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V",
                                                   javaPostApi.object<jstring>(),
                                                   javaPassWord.object<jstring>(),
                                                   javaPhone.object<jstring>(),
                                                   javaMessage.object<jstring>());
            }


        }

        m_networkManager.clearAccessCache();
    } else {
        m_connectionErrorTimer->stop();
        m_connectionAttempts = 0;
        //parse the reply JSON and display result in the UI

        m_jsonObject = QJsonDocument::fromJson(reply->readAll()).object();

        if (m_jsonObject.contains("students") && m_jsonObject["students"].isArray())
        {
            QJsonArray studentsArray = m_jsonObject["students"].toArray();

            for (int studentsIndex = 0; studentsIndex<studentsArray.size(); ++studentsIndex) {
                QJsonObject studentsObject = studentsArray[studentsIndex].toObject();

                QString msgBoddy;

                if (studentsObject.contains("name") && studentsObject["name"].isString()
                        && studentsObject.contains("mobile") && studentsObject["mobile"].isString()){
                    emit jsonObjectChanged(studentsObject["name"].toString());
                    msgBoddy = QString("Prière de payer les frais mensuelle de votre enfant %1.").arg(studentsObject["name"].toString());


                    QAndroidJniObject javaPostApi = QAndroidJniObject::fromString(m_postApi);
                    QAndroidJniObject javaPassWord = QAndroidJniObject::fromString(m_passWord);
                    QAndroidJniObject javaName = QAndroidJniObject::fromString(studentsObject["name"].toString());
                    QAndroidJniObject javaPhone = QAndroidJniObject::fromString(studentsObject["mobile"].toString());
                    QAndroidJniObject javaMessage = QAndroidJniObject::fromString(msgBoddy);
                    QAndroidJniObject::callStaticMethod<void>("com/school/srvice/MyService",
                                                       "sendSms",
                                                       "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V",
                                                       javaPostApi.object<jstring>(),
                                                       javaPassWord.object<jstring>(),
                                                       javaPhone.object<jstring>(),
                                                       javaMessage.object<jstring>(),
                                                       javaName.object<jstring>());
                    QThread::sleep(4);
                }

            }
            QAndroidJniObject javaNotification = QAndroidJniObject::fromString("La liste des etudiant qui n'ont pas payé les frais mensuelle");
            QAndroidJniObject::callStaticMethod<void>("com/school/srvice/MyService",
                                               "notify",
                                               "(Ljava/lang/String;)V",
                                               javaNotification.object<jstring>());
        }


    }

    reply->deleteLater();



}

void httpRequest::readConfiguration()
{
     QFile configurationFile(QStandardPaths::displayName(QStandardPaths::AppConfigLocation) + "configuration.json");
     if (configurationFile.open(QIODevice::ReadOnly)) {
        QByteArray confBytes = configurationFile.readAll();
        QJsonObject confObject = QJsonDocument::fromJson(confBytes).object();
        if (confObject.contains("get") && confObject["get"].isString() &&
            confObject.contains("post") && confObject["post"].isString() &&
            confObject.contains("day") && confObject["day"].isDouble() &&
            confObject.contains("passWord") && confObject["passWord"].isString() &&
            confObject.contains("userPhone") && confObject["userPhone"].isString() &&
            confObject.contains("techPhone") && confObject["techPhone"].isString())
        {
            m_getApi = confObject["get"].toString();
            m_postApi = confObject["post"].toString();
            m_day = confObject["day"].toInt();
            m_passWord = confObject["passWord"].toString();
            m_userPhone = confObject["userPhone"].toString();
            m_techPhone = confObject["techPhone"].toString();


        }else
        {
            m_getApi = "Defaul get url";
            m_postApi = "Default post url to signal an error";
            m_day = 6;
            m_passWord = "token or password in the header of the request";
            m_userPhone = "user phone";
            m_techPhone = "technician phone";
            qWarning("Couldn't open save file. The default configuration will be used");
        }



     }else
     {
         m_getApi = "Defaul get url";
         m_postApi = "Default post url to signal an error";
         m_day = 6;
         m_passWord = "token or password in the header of the request";
         m_userPhone = "user phone";
         m_techPhone = "technician phone";
         qWarning("Couldn't open save file. The default configuration will be used");
     }
}

QJsonObject httpRequest::getRequest()
{

    return m_jsonObject;
}


