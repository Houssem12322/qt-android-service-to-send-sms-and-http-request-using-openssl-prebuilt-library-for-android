#ifndef HTTPREQUEST_H
#define HTTPREQUEST_H

#include <QObject>
#include "QTimer"
#include "QtNetwork"
#include <QAndroidJniObject>
#include <QAndroidJniEnvironment>
#include <QtAndroid>

class httpRequest : public QObject
{
    Q_OBJECT
public:
    explicit httpRequest(QObject *parent = nullptr);
    static bool internetConection();
    void readConfiguration();

signals:
    void jsonObjectChanged(const QString &name);

public slots:
    QJsonObject getRequest();
    void shouldRequest();
    void treatRequestResult(QNetworkReply* reply);

private:
    QTimer *m_connectionErrorTimer;
    QTimer *m_timer;
    QDate m_currentDate;
    QTime m_currentTime;
    int m_connectionAttempts;
    QNetworkAccessManager m_networkManager;
    QNetworkRequest m_networkRequest;
    QJsonObject m_jsonObject;
    QString m_getApi;
    QString m_postApi;
    int m_day;
    QString m_passWord;
    QString m_userPhone;
    QString m_techPhone;
};

#endif // HTTPREQUEST_H
