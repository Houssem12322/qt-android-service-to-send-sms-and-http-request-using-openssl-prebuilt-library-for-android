#include "tools.h"

tools::tools(QObject *parent) : QObject(parent)
{

}

bool tools::internetConection(){
    QNetworkAccessManager nam;
    QNetworkRequest req(QUrl("http://www.google.com"));
    QNetworkReply *reply = nam.get(req);
    QEventLoop loop;
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();
    if(reply->bytesAvailable())
    {
        return true;
    }
    else
    {
        return false;
    }
}

void tools::testService(const QString &getUrl, const QString &postUrl, const QString &day, const QString &passWord, const QString &userPhone, const QString &techPhone) {


    if(!getUrl.isEmpty() && !postUrl.isEmpty() && !day.isEmpty()  &&
            !userPhone.isEmpty() && !techPhone.isEmpty() && !passWord.isEmpty())
    {
       QNetworkRequest networkRequest;
       networkRequest.setUrl(QUrl(getUrl));
       networkRequest.setRawHeader(QByteArray("Authorization"), passWord.toUtf8());
       QNetworkAccessManager testNetworkManger;
       QNetworkReply *reply =  testNetworkManger.get(networkRequest);
       QEventLoop loop;
       connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
       loop.exec();

       if(reply->error() != QNetworkReply::NoError){


           if(internetConection())
           {
               QAndroidJniObject javaPostApi = QAndroidJniObject::fromString(postUrl);
               QAndroidJniObject javaPassWord = QAndroidJniObject::fromString(passWord);
               QAndroidJniObject javaPhone = QAndroidJniObject::fromString(techPhone);
               QAndroidJniObject javaMessage = QAndroidJniObject::fromString("Can't establish a connection to the API");
               QAndroidJniObject::callStaticMethod<void>("com/kdab/training/MyService",
                                                  "sendSms",
                                                  "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V",
                                                  javaPostApi.object<jstring>(),
                                                  javaPassWord.object<jstring>(),
                                                  javaPhone.object<jstring>(),
                                                  javaMessage.object<jstring>(),
                                                  QtAndroid::androidContext().object());
               emit configurationState("Please check the URL or the server");

           }else
           {
               QAndroidJniObject javaPostApi = QAndroidJniObject::fromString(postUrl);
               QAndroidJniObject javaPassWord = QAndroidJniObject::fromString(passWord);
               QAndroidJniObject javaPhone = QAndroidJniObject::fromString(userPhone);
               QAndroidJniObject javaMessage = QAndroidJniObject::fromString("Veuillez bien vérifier le mobile de diffusion de SMS");
               QAndroidJniObject::callStaticMethod<void>("com/kdab/training/MyService",
                                                  "sendSms",
                                                  "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V",
                                                  javaPostApi.object<jstring>(),
                                                  javaPassWord.object<jstring>(),
                                                  javaPhone.object<jstring>(),
                                                  javaMessage.object<jstring>(),
                                                  QtAndroid::androidContext().object());
               emit configurationState("No internet connexion ");

           }




           testNetworkManger.clearAccessCache();
       } else {

           QJsonObject jsonObject = QJsonDocument::fromJson(reply->readAll()).object();

           if (jsonObject.contains("students") && jsonObject["students"].isArray())
           {
               QJsonArray studentsArray = jsonObject["students"].toArray();

               for (int studentsIndex = 0; studentsIndex<studentsArray.size(); ++studentsIndex) {
                   QJsonObject studentsObject = studentsArray[studentsIndex].toObject();

                   QString msgBoddy;

                   if (studentsObject.contains("name") && studentsObject["name"].isString()
                           && studentsObject.contains("mobile") && studentsObject["mobile"].isString()){

                       msgBoddy = QString("Prière de payer les frais mensuelle de votre enfant %1.").arg(studentsObject["name"].toString());


                       QAndroidJniObject javaPostApi = QAndroidJniObject::fromString(postUrl);
                       QAndroidJniObject javaPassWord = QAndroidJniObject::fromString(passWord);
                       QAndroidJniObject javaName = QAndroidJniObject::fromString(studentsObject["name"].toString());
                       QAndroidJniObject javaPhone = QAndroidJniObject::fromString(studentsObject["mobile"].toString());
                       QAndroidJniObject javaMessage = QAndroidJniObject::fromString(msgBoddy);
                       QAndroidJniObject::callStaticMethod<void>("com/kdab/training/MyService",
                                                          "sendSms",
                                                          "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V",
                                                          javaPostApi.object<jstring>(),
                                                          javaPassWord.object<jstring>(),
                                                          javaPhone.object<jstring>(),
                                                          javaMessage.object<jstring>(),
                                                          javaName.object<jstring>(),
                                                          QtAndroid::androidContext().object());
                       QThread::sleep(4);
                   }

               }
               QAndroidJniObject javaNotification = QAndroidJniObject::fromString("La liste des etudiant qui n'ont pas payé les frais mensuelle");
               QAndroidJniObject::callStaticMethod<void>("com/kdab/training/MyService",
                                                  "notify",
                                                  "(Ljava/lang/String;Landroid/content/Context;)V",
                                                  javaNotification.object<jstring>(),
                                                  QtAndroid::androidContext().object());
           }
           emit configurationState("Done");


       }

       reply->deleteLater();


    }else
    {
        emit configurationState("Please fill all the fields");
    }



}

void tools::startService(const QString &getUrl, const QString &postUrl, const QString &day, const QString &passWord, const QString &userPhone, const QString &techPhone) {

    qDebug()<< day.toInt();
    QFile configurationFile(QStandardPaths::displayName(QStandardPaths::AppConfigLocation) + "configuration.json");
    if(!getUrl.isEmpty() && !postUrl.isEmpty() && !day.isEmpty()  &&
            !userPhone.isEmpty() && !techPhone.isEmpty() && !passWord.isEmpty())
    {
        QJsonObject configurationObject;
        configurationObject.insert("get", QJsonValue(getUrl));
        configurationObject.insert("post", QJsonValue(postUrl));
        configurationObject.insert("day", QJsonValue(day.toInt()));
        configurationObject.insert("passWord", QJsonValue(passWord));
        configurationObject.insert("userPhone", QJsonValue(userPhone));
        configurationObject.insert("techPhone", QJsonValue(techPhone));


        if (!configurationFile.open(QIODevice::Truncate | QIODevice::WriteOnly)) {
                qWarning("Couldn't open json file.");
                emit configurationState("Error when opening the configuration file");
        }
        else
        {
            QJsonDocument jsonDocument(configurationObject);
            if (configurationFile.write(jsonDocument.toJson()) != -1)
            {
                if(configurationFile.flush())
                {
                    configurationFile.close();
                    QAndroidJniObject::callStaticMethod<void>("com/kdab/training/MyService",
                                                              "startMyService",
                                                              "(Landroid/content/Context;)V",
                                                              QtAndroid::androidActivity().object());
                    emit configurationState("The service is started using the given configuration");

                }else
                {
                    emit configurationState("Could not start the servcie (error when writing the configuration)");
                }
            }else
            {
                configurationFile.remove();
                emit configurationState("Could not start the servcie (error when writing the configuration)");
            }
        }

    }else
    {
        emit configurationState("Please fill all the fields");
    }



}

void tools::startServiceFromOld()
{
    QFile configurationFile(QStandardPaths::displayName(QStandardPaths::AppConfigLocation) + "configuration.json");
    if(configurationFile.exists() )
    {
        QAndroidJniObject::callStaticMethod<void>("com/kdab/training/MyService",
                                                  "startMyService",
                                                  "(Landroid/content/Context;)V",
                                                  QtAndroid::androidActivity().object());
        emit configurationState("Service is started using the old configuration");
    }else
    {
        emit configurationState("There is no old configuration");
    }

}
