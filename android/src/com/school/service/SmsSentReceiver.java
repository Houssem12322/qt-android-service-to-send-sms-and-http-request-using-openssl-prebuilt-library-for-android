package com.school.service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.telephony.SmsManager;


public class SmsSentReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {

        if(getResultCode() != android.app.Activity.RESULT_OK)
        {
            NotificationManager m_notificationManager;
            m_notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
            PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                    new Intent(context, org.qtproject.qt5.android.bindings.QtActivity.class), 0);

            // Set the info for the views that show in the notification panel.
            Notification notification = new Notification.Builder(context)
                    .setSmallIcon(R.drawable.icon)  // the status icon
                    .setWhen(System.currentTimeMillis())  // the time stamp
                    .setContentTitle("A message from school application")  // the label of the entry
                    .setContentText(intent.getStringExtra("date"))  // the contents of the entry
                    .setContentIntent(contentIntent)  // The intent to send when the entry is clicked
                    .build();
            notification.defaults |= Notification.DEFAULT_SOUND;
            notification.defaults |= Notification.DEFAULT_VIBRATE;

            // Send the notification.
            m_notificationManager.notify(1, notification);
            String errorString = new String();
            switch (getResultCode())
            {
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    errorString = "RESULT_ERROR_GENERIC_FAILURE";
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    errorString = "RESULT_ERROR_NO_SERVICE";
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    errorString = "RESULT_ERROR_NULL_PDU";
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    errorString = "RESULT_ERROR_RADIO_OFF";
                    break;
            }

            PostRequestThread postThread = new PostRequestThread(intent.getStringExtra("postApi"), intent.getStringExtra("passWord"),
                                                                 intent.getStringExtra("phone"), intent.getStringExtra("date"), errorString);
            postThread.start();

          }

        }





}
