package com.school.service;

import javax.net.ssl.HttpsURLConnection;
import org.json.JSONObject;
import java.net.URL;
import java.io.OutputStream;
import java.io.InputStreamReader;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.OutputStreamWriter;
import org.json.JSONException;
import android.util.Log;
import java.net.MalformedURLException;
import java.io.IOException;
import java.lang.Thread;
import java.lang.Thread;


public class PostRequestThread extends Thread{

    private String m_phone;
    private String m_date;
    private String m_error;
    private String m_postApi;
    private String m_passWord;
    PostRequestThread(String postApi, String passWord, String phone, String date, String error) {
        m_phone = phone;
        m_date = date;
        m_error = error;
        m_postApi = postApi;
        m_passWord = passWord;
    }

    public void run() {
        HttpsURLConnection urlConnection = null;
        JSONObject smsLogJson = new JSONObject();
        String stringFile = new String();
        URL url;

        try {

          smsLogJson.put("date", m_date);
          smsLogJson.put("err", m_error);
          smsLogJson.put("number", m_phone);
          Log.d("postThread", smsLogJson.toString());

          url=new URL(m_postApi);

          urlConnection = (HttpsURLConnection) url.openConnection();
          urlConnection.setRequestProperty("Authorization", m_passWord);
          urlConnection.setRequestMethod("POST");
          urlConnection.setDoOutput(true);
          urlConnection.setChunkedStreamingMode(0);
          //urlConnection.connect();

          OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
          OutputStreamWriter outWriter = new OutputStreamWriter(out, "UTF-8");
          outWriter.write(smsLogJson.toString());
          outWriter.flush();
          outWriter.close();

          BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(),"utf-8"));
          String line = null;

          StringBuilder stringBuilder = new StringBuilder();
          while ((line = in.readLine()) != null)
          {
              stringBuilder.append(line + "\n");
          }
          stringFile = stringBuilder.toString();

          in.close();

        }
        catch(MalformedURLException urlException){
            Log.d("smsReceiver" ,"url");
        }
        catch(IOException ioException){
            Log.d("smsReceiver" ,"IO");
        }
        catch(JSONException jsonException){
            Log.d("smsReceiver" ,"json");
        }

        finally {
            if(urlConnection != null)
                urlConnection.disconnect();
            Log.d("smsReceiver" ,stringFile);
        }
    }

}
