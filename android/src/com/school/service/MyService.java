package com.school.service;
import java.util.Calendar;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import org.qtproject.qt5.android.bindings.QtService;
import android.telephony.SmsManager;
import android.app.PendingIntent;
import android.support.v4.content.ContextCompat;
import android.support.v4.app.ActivityCompat;
import android.Manifest;
import android.content.pm.PackageManager;
import android.app.Activity;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.DateFormat;

public class MyService extends QtService
{
    private static MyService cxt;
    private static Context mainContext;
    private static Activity mainActivity;


    @Override
   public void onCreate()
   {
       super.onCreate();
       cxt = this;
   }

    public static void notify(String s, Context context)
    {
        Intent intent = new Intent();
        intent.setAction("com.school.srvice.NOTIFICATION");
        intent.putExtra("message",s);
        context.sendBroadcast(intent);

    }

    public static void notify(String s)
    {
        notify(s, cxt);
    }



    public static void sendSms(String postApi, String passWord, String phone, String msg, String name, Context context)
    {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        // Get the date today using Calendar object.
        Date now = Calendar.getInstance().getTime();
        String nowString = df.format(now);
        //Getting intent and PendingIntent instance
        Intent sentIntent = new Intent();
        sentIntent.setAction("com.school.srvice.SMS_SENT");

        sentIntent.putExtra("phone", phone);
        sentIntent.putExtra("date", nowString);
        sentIntent.putExtra("postApi", postApi);
        sentIntent.putExtra("passWord", passWord);
        PendingIntent sentPi=PendingIntent.getBroadcast(context, 0, sentIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        //Get the SmsManager instance and call the sendTextMessage method to send message
        SmsManager sms=SmsManager.getDefault();
        sms.sendTextMessage(phone, null, msg, sentPi, null);
    }
    public static void sendSms(String postApi, String passWord, String phone, String msg)
    {
        sendSms(postApi, passWord, phone, msg, "", cxt);
    }

    public static void sendSms(String postApi, String passWord, String phone, String msg, Context context)
    {
        sendSms(postApi, passWord, phone, msg, "", context);
    }
    public static void sendSms(String postApi, String passWord, String phone, String msg, String name)
    {
        sendSms(postApi, passWord, phone, msg, name, cxt);
    }



    public static void requestePermission(Activity act, Context ctx)
    {
        mainContext = ctx;
        mainActivity = act;
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(ctx,
                Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(act,
                    Manifest.permission.SEND_SMS)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(act,
                        new String[]{Manifest.permission.SEND_SMS},
                        1);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            //act.finich();
            // Permission has already been granted
        }




    }

    public static void startMyService(Context ctx) {
        ctx.startService(new Intent(ctx, com.school.service.MyService.class));
    }
}
