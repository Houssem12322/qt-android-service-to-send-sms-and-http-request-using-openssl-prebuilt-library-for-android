import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.11


Page{

    id:page
    Dialog {
        id: stateDialog

        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        width: page.width * 4/5
        title: "Message"

        Label {
            id:stateLabel
            text: "Error"
            anchors.fill: parent
            wrapMode: Label.Wrap

        }
    }

    Connections{
        target: tools
        onConfigurationState:{
            stateLabel.text = msg
            stateDialog.open()
        }
    }
    Flickable {
        id:flickContainer
        anchors.fill: parent

        //contentWidth: col.width // you can use whatever you want, this is just an example
        contentHeight: col.height
        ColumnLayout {
            id: col
            width: parent.width
            anchors.horizontalCenter: parent.horizontalCenter


            TextField {
                id:getApi
                Layout.maximumWidth: page.width * 8/9
                Layout.fillWidth: true
                placeholderText : "The API Url "
                Layout.margins: 5;
                Layout.alignment:Qt.AlignHCenter

            }

            TextField {
                id: logApi
                Layout.maximumWidth: page.width * 8/9
                Layout.fillWidth: true
                placeholderText : "The Log API Url"
                Layout.margins: 5;
                Layout.alignment:Qt.AlignHCenter

            }

            TextField {
                id: day
                Layout.maximumWidth: page.width * 8/9
                Layout.fillWidth: true
                placeholderText : "Day"
                Layout.margins: 5;
                Layout.alignment:Qt.AlignHCenter
                inputMethodHints:Qt.ImhDigitsOnly


            }
            TextField {
                id: passWord
                Layout.maximumWidth: page.width * 8/9
                Layout.fillWidth: true
                placeholderText : "Password"
                Layout.margins: 5;
                Layout.alignment:Qt.AlignHCenter

            }
            TextField {
                id: userPhone
                Layout.maximumWidth: page.width * 8/9
                Layout.fillWidth: true
                placeholderText : "User phone"
                Layout.margins: 5;
                Layout.alignment:Qt.AlignHCenter
                inputMethodHints:Qt.ImhDigitsOnly


            }
            TextField {
                id: techPhone
                Layout.maximumWidth: page.width * 8/9
                Layout.fillWidth: true
                placeholderText : "Technicien phone"
                Layout.margins: 5;
                Layout.alignment:Qt.AlignHCenter
                inputMethodHints:Qt.ImhDigitsOnly

            }
            RowLayout
            {
                spacing: 10;
                Layout.alignment:Qt.AlignHCenter

                Button {
                   id: test
                   text: "Test the SMS srrvice"
                   Layout.maximumWidth: page.width * 2/7
                   contentItem: Label {
                             text: test.text
                             font: test.font
                             verticalAlignment: Text.AlignVCenter
                             wrapMode: Label.Wrap
                         }
                   onClicked: confirmationDialogTest.open()
                   Dialog {
                       id: confirmationDialogTest
                       width: page.width * 4/5
                       x: (parent.width - width) / 2
                       y: (parent.height - height) / 2
                       parent: Overlay.overlay

                       modal: true
                       title: "Confirmation"
                       standardButtons: Dialog.Yes | Dialog.No

                       Column {
                           spacing: 20
                           anchors.fill: parent
                           Label {
                               width: parent.width
                               text: "This action will send SMS to parents. Do you want to continue"
                                wrapMode: Label.Wrap
                           }

                       }

                       onAccepted: tools.testService(getApi.text, logApi.text, day.text, passWord.text, userPhone.text, techPhone.text)
                 }
               }

                Button {
                   id: start
                   text: "Start the service"
                   Layout.maximumWidth: page.width * 2/7
                   contentItem: Label {
                             text: start.text
                             font: start.font
                             verticalAlignment: Text.AlignVCenter
                             wrapMode: Label.Wrap
                         }
                   onClicked: confirmationDialog.open()
                   Dialog {
                       id: confirmationDialog
                       width: page.width * 4/5
                       x: (parent.width - width) / 2
                       y: (parent.height - height) / 2
                       parent: Overlay.overlay

                       modal: true
                       title: "Confirmation"
                       standardButtons: Dialog.Yes | Dialog.No

                       Column {
                           spacing: 20
                           anchors.fill: parent
                           Label {
                               width: parent.width
                               text: "Are you sure you want to start the service"
                                wrapMode: Label.Wrap
                           }

                       }

                       onAccepted: tools.startService(getApi.text, logApi.text, day.text, passWord.text, userPhone.text, techPhone.text)
                 }
               }
               Button {
                  id:startO
                  text: "Start the old conf"
                  Layout.maximumWidth: page.width * 2/7
                  contentItem: Label {
                            text: startO.text
                            font: startO.font
                            verticalAlignment: Text.AlignVCenter
                            wrapMode: Label.Wrap
                        }
                  onClicked: confirmationDialogOld.open()
                  Dialog {
                      id: confirmationDialogOld
                      width: page.width * 4/5
                      x: (parent.width - width) / 2
                      y: (parent.height - height) / 2
                      parent: Overlay.overlay

                      modal: true
                      title: "Confirmation"
                      standardButtons: Dialog.Yes | Dialog.No

                      Column {
                          spacing: 20
                          anchors.fill: parent
                          Label {
                              width:parent.width
                              text: "Are you sure you want to start the service from the old configuration"
                              wrapMode: Label.Wrap
                          }

                      }

                      onAccepted: tools.startServiceFromOld()
                }


            }


            }
        }
    }
}

